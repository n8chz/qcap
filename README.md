This is in a very early stage of development. It is not ready for use.

**Quadratic assignment problems** (QAP) are attempts to minimize sum of flow times distance over edges of a graph whose vertices represent things we would like to be near to each other in the case of positive flow between them, and far from each other in the case of negative flow.

This package is an attempt to optimize for quadratic assignment subject to the constraint that the nodes represented by the vertices of the flow graph are assumed to be circles whose radii are among the givens in a quadratic assignment problem. If nodes v<sub>1</sub> and v<sub>2</sub> have radii r<sub>1</sub> and r<sub>2</sub>, layouts with dist(v<sub>1</sub>, v<sub>2</sub>) &lt; r<sub>1</sub>+r<sub>2</sub> are not included in the solution space.

See Castillo, I and Sim, T, 2003, *[A Spring-Embedding Approach for the Facility Layout Problem](http://www.optimization-online.org/DB_FILE/2002/12/577.pdf)*.
