import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="swar",
    scripts = ["src/swar.py"],
    version="0.0.1",
    author="Lorraine Lee",
    author_email="lori@astoundingteam.com",
    description="Circle QAP",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab/n8chz/qcap_pkg",
    package_dir = {"": "src"},
    packages=setuptools.find_packages(where = "src"),
    license = "Hippocratic",
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.8.2',
    tests_require = ["pytest"],
)

