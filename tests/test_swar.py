import pytest
import sys
import os
from src.swar import Swar
import numpy as np

sys.path.append(os.path.abspath("../src"))
os.chdir("src")

class TestSwar:
    # @pytest.mark.skip()
    def test_init(self):
        os.system("ruby generate_swar.rb > test.swar")
        swar = Swar("test.swar")
        os.system("rm test.swar")
        # h/t akaihola https://stackoverflow.com/a/41830285/948073
        pytest.assume(isinstance(swar.nodes, list))
        pytest.assume(isinstance(swar.flow, np.ndarray))
        pytest.assume(isinstance(swar.radii, np.ndarray))
        pytest.assume(isinstance(swar.locations, np.ndarray))
        pytest.assume(isinstance(swar.dist, np.ndarray))

    # @pytest.mark.skip()
    def test_objective(self):
        os.system("ruby generate_swar.rb > test.swar")
        swar = Swar("test.swar")
        os.system("rm test.swar")
        pytest.assume(isinstance(swar.objective(), float))

    def test_normalize(self):
        os.system("ruby generate_swar.rb > test.swar")
        swar = Swar("test.swar")
        os.system("rm test.swar")
        # print(str(swar.locations))
        swar.normalize()
        # print(str(swar.locations))
        pytest.assume(isinstance(swar.locations, np.ndarray))

    # @pytest.mark.skip()
    def test_svg(self):
        os.system("ruby generate_swar.rb > test.swar")
        swar = Swar("test.swar")
        os.system("rm test.swar")
        s = swar.svg()
        print(s)
        pytest.assume(s, str)



