import numpy as np
import re

# see http://www.optimization-online.org/DB_FILE/2002/12/577.pdf

class Swar:
    def __init__(self, file_name):
        self.__r = {}
        with open(file_name, "r") as file_in:
            self.nodes = set()
            self.__flows = {}
            for line in file_in:
                self.__add_line(line)
        self.nodes = list(self.nodes)
        n = len(self.nodes)
        self.radii = np.ones(n)
        # establish radii of nodes
        # these are constant values
        for i, n1 in enumerate(self.nodes):
            if n1 in self.__r:
                self.radii[i] = self.__r[n1]
        # assign nodes to random locations to initialize
        self.locations = np.random.uniform(-1.0, 1.0, (n, 2))
        self.nominal = np.zeros((n*(n-1)//2))
        self.flow = np.zeros((n*(n-1)//2))
        k = 0
        for j in range(1, n):
            for i in range(j):
                self.nominal[k] = self.radii[i]+self.radii[j]
                pair = (self.nodes[i], self.nodes[j])
                if pair in self.__flows:
                    self.flow[k] = self.__flows[pair]
                k += 1
        self.dist = np.zeros((n*(n-1)//2))
        # self.measure()


    def measure(self):
        n = len(self.nodes)
        k = 0
        for j in range(1, n):
            for i in range(j):
                self.dist[k] = np.linalg.norm(self.locations[i]-self.locations[j])
                k += 1

    def center(self):
        centroid = np.sum(self.locations, 0)/len(self.nodes)
        self.locations -= centroid
        
    def normalize(self):
        self.center()
        self.measure()
        # TODO: cover both expansion and contraction cases
        # expansion case
        # print(self.nominal)
        # print(self.dist)
        # print(self.nominal/self.dist)
        k = np.amax(self.nominal/self.dist)
        print(k)
        if k < 1:
            k = np.amin(self.nominal/self.dist)
        self.locations *= k


    def objective(self):
        self.normalize()
        self.measure()
        return np.sum(self.flow*self.dist)
            
    def frame(self):
        self.normalize()
        xmin = 1000000
        ymin = 1000000
        xmax = -1000000
        ymax = -1000000
        for i, n in enumerate(self.nodes):
            x, y = self.locations[i]
            r = self.radii[i]
            xmin = min(xmin, x-r)
            ymin = min(ymin, y-r)
            xmax = max(xmax, x+r)
            ymax = max(ymax, y+r)
        return [xmin, ymin, xmax, ymax]

    def svg(self):
        svg = "<svg viewBox=\""
        svg += " ".join(map(str, self.frame()))
        svg += "\" xmlns=\"http://www.w3.org/2000/svg\">"
        for i, node in enumerate(self.nodes):
            cx, cy = self.locations[i]
            r = self.radii[i]
            svg += "<circle cx=\""
            svg += str(cx)
            svg += "\" cy=\""
            svg += str(cy)
            svg += "\" r=\""
            svg += str(r)
            svg += "\"/>"
        svg += "</svg>"
        return svg

    def __add_line(self, line):
        tokens = re.split(r"\s+", line.lower().strip())
        if tokens == [""]:
            return
        if tokens[0] == "/":
            try:
                radius = float(tokens[1])
            except:
                print("item after \"/\" directive must be a number.")
                raise SyntaxError
            for node in tokens[2:]:
                self.__r[node] = radius
            return
        operators = set(tokens)&{"sa", "sr", "wa", "wr"}
        if len(operators) > 1:
            print("Multiple operators on one line: ", operators)
            raise SyntaxError
        if len(operators) == 0:
            print("Line contains no operator: ", line)
            raise SyntaxError
        operator = operators.pop()
        flow = (2 if operator[0] == "s" else 1)*(1 if operator[1] == "a" else -1)
        i = tokens.index(operator)
        left_tokens = tokens[:i]
        right_tokens = tokens[i+1:]
        self.nodes.update(left_tokens)
        self.nodes.update(right_tokens)
        for lt in left_tokens:
            for rt in right_tokens:
                pair = (lt, rt)
                if pair in self.__flows:
                    self.__flows[pair] += flow
                else:
                    self.__flows[pair] = flow
