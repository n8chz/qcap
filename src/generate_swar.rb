radii = {}
rand(10..30).times do
  nodes = ("a".."z").to_a.sample(rand(2..8))
  nodes.each do |node|
    if [true, false].sample
      if radii[node].nil?
        radius = rand(1..9)
        radii[node] = radius
        puts "/ #{radius} #{node}"
      end
    end
  end
  pos = rand(1..nodes.count-1)
  operator = ['sa', 'sr', 'wa', 'wr'].sample
  puts nodes.insert(pos, operator).join(" ")
  puts if rand(0..5).zero?
end
